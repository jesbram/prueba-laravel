<?php

namespace App\Http\Controllers;

use App\users;
use App\cargos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $users=users::orderBy("id","DESC")->paginate(3);
        $cargos=new cargos;

        return view("templates.table_users",compact("users","cargos"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        #$users=users::all();
        $users=new users;
        $cargos = cargos::all('id',"cargo");
        
        
        return view("templates.create_users",compact("users","cargos"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $user=new users();
        $user->name=$request->input("name");
        $user->cedula=$request->input("cedula");
        $user->email=$request->input("email");
        #$user->password=$request->input("password");
        $user->cargo_id=$request->input("cargo");
        $user->save();
        return redirect("/users");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function show(users $users)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $user=users::where("id",$id)->first();
        $cargos = cargos::all('id',"cargo");
        return view("templates.edit_users",compact("user","cargos"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        //
        $user=users::where("id",$id)->first();
        $user->name=$request->input("name");
        $user->cedula=$request->input("cedula");
        $user->email=$request->input("email");
        #$user->password=$request->input("password");
        $user->cargo_id=$request->input("cargo");
        $user->save();
        return redirect("/users");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\users  $users
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $user=users::where("id",$id)->first();
        $user->delete();
        return redirect("/users");
    }
}

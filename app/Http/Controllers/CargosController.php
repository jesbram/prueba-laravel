<?php

namespace App\Http\Controllers;
use App\cargos;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CargosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cargos=cargos::orderBy("id","DESC")->paginate(15);
        return view("templates.table_cargos",compact("cargos"));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cargos=new cargos;

        return view("templates.create_cargos",compact("cargos"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //
        $data=$request->all();
        
        $rules=[
            "cargo"=>"unique:cargos|required",
            "_token"=>"required"];
        $message=[
            "required"=>"el campo :attribute es requerido"
        ];
        $validator=Validator::make($data,$rules);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator->errors());

        }
        $cargos=new cargos();
        $cargos->cargo=$request->input("cargo");
        $cargos->save();
        return redirect("/cargos");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        // 

        $cargo=cargos::where("id",$id)->first();
        
        
        

        return view("templates.edit_cargos",compact("cargo"));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $cargo=cargos::where("id",$id)->first();
        $cargo->cargo=$request->input("cargo");
        $cargo->save();
        return redirect("/cargos");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cargo=cargos::where("id",$id)->first();
        $cargo->delete();
        return redirect("/cargos");
    }
}

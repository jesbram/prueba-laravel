<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	dd("UserSeeder");
        // $this->call(UsersTableSeeder::class);
        $this->call(UserSeeder::class);
    }
}

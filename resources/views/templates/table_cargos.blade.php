@extends("layout")
@section("content")

<div class="container" style="margin-top: 100px">
<div class="row">
	<div class="col-md-12">
		<a class="btn btn-secondary" href="/">Regresar al menu</a>
		<a class="btn btn-primary" href="/cargos/create">Añadir cargo</a>
		
		
	</div>
	
</div>
<div class="container">
	<div class="row">

		<div class="col-md-12">
			{{ $cargos->links() }}
			<div class="table-responsive">
		<table class="table table-hover table-striped">
			<thead>
			<tr>
				<th>ID</th>
				<th>Cargo</th>
			</tr>
			</thead>
			<tbody>
			@if (!empty($cargos))
			@foreach ($cargos as $cargo)
			<tr>
				<td>{{$cargo->id}}</td>	
				<td>{{$cargo->cargo}}</td>	
				<td><a href="/cargos/{{$cargo->id}}/edit" class="btn btn-warning">Editar</a></td>	
				<td>
					{!!Form::model($cargos,["action"=>["CargosController@destroy",$cargo->id]])!!}
					@method("DELETE")
					<button class="btn btn-danger">Borrar</button>
					
					{!!Form::close()!!}

				</td>	
			</tr>
			@endforeach
			</tbody>
			@endif

		</table>
		</div>
		{{ $cargos->links() }}
	</div>
</div>
@endsection
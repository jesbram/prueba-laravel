@extends("layout")
@section("title","Usuarios registrados")

@section("content")
<div class="container" style="margin-top: 100px">
<div class="row">
	<div class="col-md-12">
		<a class="btn btn-secondary" href="/">Regresar al menu</a>
		@if (!empty($cargos))
		<a class="btn btn-primary" href="/users/create">Añadir usuario</a>
	</div>
	
</div>
<div class="container">
	<div class="row">

		<div class="col-md-12">
			{{ $users->links() }}
			<div class="table-responsive">
				
			
		<table class="table table-hover table-striped">
			<thead >
			<tr>
				<th>ID</th>
				<th>Nombre</th>
				<th>Correo</th>
				<th>Cedula</th>
				<th>Cargo</th>
				<th></th>
				<th></th>
			</tr>
			</thead>
			@if (!empty($users))
			<tbody>
			@foreach ($users as $user)
			<tr>
				<td>{{$user->id}}</td>	
				<td>{{$user->name}}</td>	
				<td>{{$user->email}}</td>	
				<td>{{$user->cedula}}</td>	
				<td>{{ $cargos::where("id",$user->cargo_id)->first()->cargo}}</td>	
				<td><a href="/users/{{$user->id}}/edit" class="btn btn-warning">Editar</a></td>	
				<td>
					{!!Form::model($users,["action"=>["UsersController@destroy",$user->id]])!!}
					@method("DELETE")
					<button class="btn btn-danger">Borrar</button>
					
					{!!Form::close()!!}
				</td>	

			</tr>
			@endforeach
			</tbody>
			@endif
		</table>
		
		
		@else
		<p>Debes almenos un cargo para crear un usuario</p>
		<a href="/cargos/create" class="btn btn-primary">crear cargo</a>
		@endif
		</div>
		{{ $users->links() }}
		</div>
	</div>
	
</div>	
</div>

@endsection
@extends("layout")

@section("title","Editar usuarios")

@section("content")

<div class="container" style="margin-top:100px">

	</div>
	<div class="row">
		<div class="col-md-12">
			<h1>Editar cargo</h1>
		</div>
		
	</div>
	<div class="row">

		{!! Form::model($user,["route"=>["users.update",$user->id],"class"=>"col-md-12"]) !!}
			@method('PUT')
			<div class="form-group">
		{!! Form::label("name","") !!}
		{!! Form::text("name",$user->name,["class"=>"form-control"]) !!}
		{!! Form::label("cedula") !!}
		{!! Form::text("cedula",$user->cedula,["class"=>"form-control"]) !!}
		{!! Form::label("email") !!}
		{!! Form::text("email",$user->email,["class"=>"form-control"]) !!}
		
		{!! Form::label("Cargo","") !!}
		
		<select class="form-control" name="cargo">
		    @foreach($cargos as $cargo) 
		    <option value="{{$cargo->id}}"
		    	@if($cargo->id==$user->cargo_id)
		    	selected="" 
		    	@endif
		    	>{{$cargo->cargo}}</option>
		    
		    

		      
		    @endforeach
		  </select>
	</div>
	
	<button class="btn btn-primary">Guardar</button>
	{!! Form::close() !!}
</div>
</div>
@endsection
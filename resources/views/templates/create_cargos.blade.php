@extends("layout")
@section("content")
<div class="container" style="margin-top: 100px">
	<div class="row">
		<div class="col-md-12"> 
			<h1>Crear cargo</h1>
		</div>
	</div>
<div class="row">
{!! Form::model($cargos,["action"=>"CargosController@store","class"=>"col-md-12"]) !!}
<div class="form-group">
	{!!Form::text("cargo","",["class"=>"form-control"])!!}
</div>
<button class="btn btn-primary">Enviar</button>
	
{!! Form::close() !!}	
</div>	
</div>


@endsection
@extends("layout")

@section("title","Editar usuarios")

@section("content")
<div class="container" style="margin-top:100px">
	<div class="row">
		<div class="col-md-12">
			<h1>Editar cargo</h1>
		</div>
		
	</div>
	<div class="row">
	{!! Form::model($cargo,["route"=>["cargos.update",$cargo->id],"class"=>"col-md-12"]) !!}
	@method('PUT')
		<div class="form-group">
	ID {!! Form::label("id",$cargo->id,["class"=>"form-control"]) !!}
	
	CARGO {!! Form::text("cargo",$cargo->cargo,["class"=>"form-control"]) !!}
	</div>
	
	<button class="btn btn-primary">Guardar</button>
	{!! Form::close() !!}	
	</div>
	
</div>
	
@endsection
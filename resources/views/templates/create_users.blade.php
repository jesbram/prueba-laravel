@extends("layout")

@section("title","Editar usuarios")

@section("content")
<div class="container">
	<div class="row">
		
	{!! Form::model($users,["action"=>"UsersController@store","class"=>"col-md-12"]) !!}
	<div class="form-group">
	{!! Form::label("name","") !!}
	{!! Form::text("name","",["class"=>"form-control"]) !!}
	{!! Form::label("cedula","") !!}
	{!! Form::text("cedula","",["class"=>"form-control"]) !!}
	{!! Form::label("email","") !!}
	{!! Form::text("email","",["class"=>"form-control"]) !!}
	
	{!! Form::label("Cargo","") !!}
	
	<select class="form-control" name="cargo">
	    @foreach($cargos as $cargo) 
	    <option value="{{$cargo->id}}">{{$cargo->cargo}}</option>
	    
	    

	      
	    @endforeach
	  </select>
	</div>
	<button class="btn btn-primary">Crear</button>
	
	{!! Form::close() !!}	
	</div>
	
</div>
	
@endsection